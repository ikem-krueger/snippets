// Define the protocol
protocol Superize
{
    func superspeed()
}

// Extend the protocol with a default implementation
extension Superize
{
    func superspeed()
    {
        print("Moving at super sonic light speed!!!")
    }
}

// Create a super vehicle with two wheels (a superbike)
struct Vehicle:Superize
{
    var wheels = 2
}

// Use the superbike
var bike = Vehicle()
bike.superspeed()
// Outputs: Moving at super sonic light speed!!!
