// declaration of a property observer with their default parameter names
var userStatusText: String {
 
    willSet {
        print("About to set status to:  \(newValue)")
    }
 
    didSet {
        if userStatusText != oldValue {
            postNewStatusNotification()
        }
    }
}

// declaration of a property observer with custom names for newValue and oldValue
var userStatusTextCustom: String {
 
    willSet(incomingStatus) {
        print("About to set status to:  \(incomingStatus)")
    }
 
    didSet(previousStatus) {
        if userStatusText != previousStatus {
            postNewStatusNotification()
        }
    }
}