# Swift by Example

This is the markdown version of [this site](http://brettbukowski.github.io/SwiftExamples/).

## Table of Contents

* [Values](#values)
* [Variables](#variables)
* [Constants](#constants)
* [Strings](#strings)
   * [Concatenation](#concatenation)
   * [Common Operations](#common-operations)
   * [Working with Characters](#working-with-characters)
   * [Substrings](#substrings)
   * [Contains](#contains)
   * [Mutating](#mutating)
* [For Loops](#for-loops)
   * [Half-Closed range operator](#half-closed-range-operator)
   * [Closed-Range operator](#closed-range-operator)
* [If/Else](#ifelse)
* [Switch](#switch)
* [Arrays](#arrays)
   * [Accessing elements](#accessing-elements)
   * [Assigning elements](#assigning-elements)
   * [Slices](#slices)
   * [Methods](#methods)
   * [Clearing](#clearing)
   * [Using a loop to create a multidimensional array](#using-a-loop-to-create-a-multidimensional-array)
* [Dictionaries](#dictionaries)
   * [Accessing and assigning elements](#accessing-and-assigning-elements)
   * [Removing](#removing)
   * [Updating](#updating)
   * [Iterating](#iterating)
   * [Getting all keys &amp; values](#getting-all-keys--values)
   * [Clearing everything](#clearing-everything)
* [Tuples](#tuples)
* [Optionals](#optionals)
* [Functions](#functions)
   * [Default parameter values](#default-parameter-values)
   * [Variadic parameters](#variadic-parameters)
   * [In-Out (pass-by-reference) parameters](#in-out-pass-by-reference-parameters)
   * [Function Types](#function-types)
   * [Nested Functions](#nested-functions)
* [Closures](#closures)
   * [Trailing Closures](#trailing-closures)
* [Enums](#enums)
   * [Raw Values](#raw-values)
* [Classes](#classes)
   * [Lazy properties](#lazy-properties)
   * [Computed properties](#computed-properties)
   * [Read-only computed properties](#read-only-computed-properties)
   * [Property Observers](#property-observers)
   * [Type Properties](#type-properties)
   * [Type Methods](#type-methods)
   * [Instance methods](#instance-methods)
   * [Inheritance](#inheritance)
   * [Initializers](#initializers)
   * [ARC and reference cycles](#arc-and-reference-cycles)
   * [Access control](#access-control)
* [Structures](#structures)
   * [Type Properties](#type-properties-1)
   * [Instance methods](#instance-methods-1)
   * [Type Methods](#type-methods-1)
* [Values and References](#values-and-references)
   * [Classes are reference types](#classes-are-reference-types)
   * [Dictionaries are copied at the point of assignment](#dictionaries-are-copied-at-the-point-of-assignment)
   * [Arrays, Strings, and Dictionaries](#arrays-strings-and-dictionaries)
* [Type Casting](#type-casting)
   * [Downcasting](#downcasting)
   * [AnyObject](#anyobject)
   * [Any](#any)
* [Class Extensions](#class-extensions)
* [Protocols](#protocols)
   * [Protocol Composition](#protocol-composition)
   * [Protocol Checking](#protocol-checking)
   * [Optional Protocol Requirements](#optional-protocol-requirements)
* [Generics](#generics)
   * [Generic Types](#generic-types)
   * [Type Constraints](#type-constraints)
   * [Associated Types](#associated-types)
* [Where clauses](#where-clauses)
* [Advanced Operators](#advanced-operators)
   * [Bitwise Operators](#bitwise-operators)
   * [Overflow Operators](#overflow-operators)
   * [Operator Functions](#operator-functions)
* [Generators](#generators)

## Values

Strings can be added with +

```swift
print("swift" + "lang")                  // swiftlang

print(1 + 1)                             // 2
```

Casting an Int to a String

```swift
print("1+1 = " + String(1 + 1))          // 1+1 = 2

print(7.0 / 3.0)                         // 2.33333333333333

print(String(7.0) + " / " + String(3.0)) // 7.0 / 3.0
```

Booleans are booleans (YES)

```swift
print(true && false)                     // false
print(true || false)                     // true
print(!true)                             // false
```

## Variables

```swift
var yo = 1.0
yo++
yo += 1
print(yo)                      // 3.0
```

Cannot redeclare variables

```swift
/* var yo = 2.0 // 'yo' previously declared here */
```

Cannot change its type

```swift
/* yo = "yo" // cannot convert the expression's type '()' to type 'Double' */
```

Declare multiple variables on one line, separated by commas

```swift
var a = 1.0, b = 2.0, c = 3.0
print(a + b + c)              // 6.0
```

Declare the var's type if not immediately assigning a value.

```swift
var hey: String
/* print(hey)                  // variable 'hey' used before being initialized */
hey = "there"
print(hey)                    // there
```

Emoji var names

```swift
var 🐮 = "ridiculous"
print(🐮)                    // ridiculous
```

## Constants

Declare constants with `let`.

A constant can appear anywhere a `var` statement can.

```swift
let s = "string"
print(s)            // string

let n = 500000000
print(n)            // 500000000

let r = 3 ^ 20 / n
print(r)            // 3
```

'let' declarations require an initializer expression

## Strings

Swift's way of representing strings as encoding-independent	Unicode characters means that programmers don't have to worry as much about encoding issues (like Ruby, Python, etc.), but common operations can be a bit tedious as a result, since you can't directly index into a string without using a Range abstraction.

```swift
import UIKit
```

### Concatenation

```swift
var str = "Hello"
var smile = "😄"
var combined = str + " " + smile
```

### Common Operations

```swift
var length = combined.characters.count // 7
print(combined.lowercaseString)
print(combined.uppercaseString)
```

### Working with Characters

```swift
for char in combined.characters {
    // Loops seven times
    print(char)
}
```

### Substrings

```swift
var trail = combined.substringWithRange(Range<String.Index>(start: str.endIndex, end: combined.endIndex))  // " 😄"
```

### Contains

```swift
if let range = str.rangeOfString("ello") {
    print("ello")                           // ello
} else {
    print("nope")
}
```

Swift has many string methods, most inherited from the rich NSString API. But (spoiler alert) you can add your own too.

```swift
extension String {
    func beginsWith (str: String) -> Bool {
        if let range = self.rangeOfString(str) {
            return range.startIndex == self.startIndex
        }
        return false
    }

    func endsWith (str: String) -> Bool {
        if let range = self.rangeOfString(str, options:NSStringCompareOptions.BackwardsSearch) {
            return range.endIndex == self.endIndex
        }
        return false
    }
}

print("find".beginsWith("f"))       // true
print("find".beginsWith("fi"))      // true
print("find".beginsWith("fin"))     // true
print("find".beginsWith("find"))    // true
print("find".beginsWith("finder"))  // false

print("find".endsWith("d"))         // true
print("find".endsWith("nd"))        // true
print("find".endsWith("ind"))       // true
print("find".endsWith("find"))      // true
print("find".endsWith(""))          // false
```

### Mutating

```swift
combined.splice("🐈".characters, atIndex: combined.rangeOfString(smile)!.startIndex)
combined.removeAtIndex(combined.rangeOfString(smile)!.startIndex)
print(combined)                    // Hello 🐈
```

## For Loops

### Half-Closed range operator

Defines a range that doesn't include the last number.

```swift
for index in 1..<5 {
    print(index)           // 1 2 3 4
}
```

### Closed-Range operator

Defines a range that includes the last number.

```swift
for index in 1...5 {
    print(index)           // 1 2 3 4 5
}
```

Traditional for loop.

```swift
for var i = 0; i < 5; i++ {
    print(i)               // 0 1 2 3 4
}
```

## If/Else

```swift
let num = 9
```

Parens are optional.	

```swift
if (num < 0) {
    print("num is negative")
} else if num < 10 {
    print("num is single-digit") // num is single-digit
} else {
    print("num is multi-digit")
}
```

There are no 'truthy' conditionals.

The condition must be a boolean expression...

```swift
if 7 % 2 == 2 {
    print("7 is even")     // IDE helpfully notes, "Will never be executed"
} else {
    print("7 is odd")      // 7 is odd
}
```

...But optionals allow for shorthand	
 
non-nil + assignment conditionals

```swift
var optionalString:String? = "Hello?"

if let a = optionalString {
    print(a)               // Hello?
}

optionalString = nil

if let b = optionalString {
    print("yep")
} else {
    print("nope")         // nope
}
```

## Switch

```swift
let i = 7

switch i {
case 1:
    print("one")
case 2:
    print("two")
case 3, 4:
    // Comma-separate multiple expressions in the same case statement.
    print("three or four")
case _ where i > 5:
    print("greater than 5")
default:
    // Default is required.
    print("less than 1")
}
```

Switch on the result of a function.

```swift
func isEven (int: Int) -> Bool {
    return int % 2 == 0
}

switch isEven(i) {
    case true: print("Even")
    case false: print("Odd")
}
```

## Arrays

```swift
import Foundation
```

Type inference FTW.

```swift
var strings = ["a", "b", "c"]
```

Declare the type of contained elements.

```swift
var strings2: [String] = ["d", "e", "f"]
```

Declare an empty array.

```swift
var strings3 = [String]()
```

Fill an array.

```swift
var strings4 = [String](count: 3, repeatedValue: "hey")
```

Arrays must contain values of a single type.	
 
Appending.

```swift
strings += ["d"]
strings.append("e")
strings += ["f", "g"]
```

Joining.

```swift
strings3 = strings + strings2
```

Checking length.

```swift
print(strings.count)                      // 7
```

### Accessing elements

```swift
print(strings[0])                         // a
print(strings.first!)                     // a
print(strings[strings.endIndex - 1])      // g
print(strings.last!)                      // g
```

### Assigning elements

```swift
strings[0] = "a"
```

### Slices

```swift
strings[0..<1] = ["a"]                       // Exclusive (basically the same as the prev assignment)
strings[0...1] = ["a", "b"]                  // Inclusive
strings[0...3]                               // ["a", "b", "c", "d"]
strings[0..<strings.endIndex]                // ["a", "b", "c", "d", "e", "f", "g"]
```

### Methods

```swift
if strings.isEmpty {
    print("empty")
} else {
    print("populated")                   // populated
}
strings.insert("a", atIndex: 0)            // Insert, not replace
print(strings.removeAtIndex(0))          // a
strings.map({
    (str: String) -> String in
    return str + "0"
})                                         // ["a0", "b0", "c0", "d0", "e0", "f0", "g0"]
strings.removeLast()
```

### Clearing

```swift
strings.removeAll(keepCapacity: false)
strings = []
```

### Using a loop to create a multidimensional array

```swift
var rows = 10, cols = 10
var dimensional = Array<Array<Int>>()
for col in 0..<10 {
    dimensional.append(Array(count: rows, repeatedValue:Int()))
}
```

## Dictionaries

```swift
import Foundation
var emptyDictionary = Dictionary<String, Float>()
```

Shorthand sugar for the same thing.

```swift
var anotherEmptyDict = [String:Float]()
```

Type inference allows you to initialize without declaring types:

```swift
var strings = [
    "a": "A",
    "b": "B",
]
```

### Accessing and assigning elements

```swift
print(strings["a"])                           // A
strings["a"] = "AZ"                             // Returns an optional string (new value that was assigned)
strings.updateValue("AX", forKey: "a")          // Returns an optional string (old value that was overwritten)
```

Optionals are returned for accessing keys.

```swift
if let a = strings["a"] {
    print(a)                                  // AX
}
```

### Removing

Setting the value to nil also removes the key.

```swift
strings["a"] = nil
```

Using removeValueForKey removes the key-val pair but returns the value that was removed (or nil if the key-val didn't exist)

```swift
strings.removeValueForKey("nope")
```

### Updating

```swift
strings.updateValue("A", forKey: "a")
strings["a"] = "A"
```

### Iterating

```swift
for (key, val) in strings {
    print("\(key): \(val)")
}

for key in strings.keys {
    print("KEY: \(key)")
}

for val in strings.values {
    print("VAL: \(val)")                      // VAL: A VAL: B
}
```

### Getting all keys & values

```swift
let keys = Array(strings.keys)
let vals = Array(strings.values)
```

### Clearing everything

```swift
strings = [:]

print(strings.count)                         // 0
```

## Tuples

```swift
var letters = ("a", "b")
```

Decomposing.

```swift
let (first, second) = letters

print(first)                      // a
print(second)                     // b
```

Underscore to ignore, just like Go.

```swift
var (a, _) = letters

print(a)                         // a
```

Or access values via index.

```swift
print(letters.0)                 // a
print(letters.1)                 // b
```

Naming elements.

```swift
var letters2 = (first: "a", second: "b")
print(letters2.first)            // a
print(letters2.second)           // b
```

## Optionals

```swift
import Foundation
```

As an alternative to multiple return values (Go) or Exceptions (every other language)	

Optionals are used to handle failure cases when	you're good and ready.

Sorta like a lightweight maybe monad, without the FP hoity-toity.

```swift
var one = "1"
var intVal = Int(one)
```

Use a if-check to determine if there's an underlying value.

```swift
if let intVal = Int(one) {
    // And then access the underlying value with !
    print(intVal)                                              // 1
}

var nope = "please"
var nopeIntVal = Int(nope)

if nopeIntVal == nil {
    print("Couldn't convert the string to an int")
}
```

Creating optionals.

```swift
var maybe:String? = nil

print(maybe)                                                  // nil
print(maybe == nil)                                           // true

maybe = "yep"

print(maybe)                                                  // yep
```

## Functions

```swift
func simple() {
    print("hi")
}

simple()                                // hi
```

Parameter types and the return type must be specified.

```swift
func plus(a: Int, b: Int) ->Int {
    return a + b
}

func sum(numbers: [Double]) -> Double {
    var sum: Double = 0.0
    for num in numbers {
        sum += num
    }
    return sum
}
```

Functions can return tuples.

```swift
func stats(numbers: [Int]) -> (min: Int, max: Int) {
    var min = Int.max, max = Int.min

    for i in numbers {
        if i < min {
            min = i
        }
        if i > max {
            max = i
        }
    }

    return (min, max)
}
```

Functions that don't return anything return `Void` (an empty tuple) by default.

```swift
func noop(){}
noop()

var result = stats([1, 2, 4])
print(result.min)                      // 1
print(result.max)                      // 4
```

Named parameters use an 'external' name before the 'internal' name.	
 
Kind of a kludgy holdover from Obj-C selectors...

```swift
func increment(number: Int, by incrementer: Int) -> Int {
    return number + incrementer
}

print(increment(1, by: 10))           // 11
```

...So fortunately there's a shorthand for re-using the same name.

```swift
func incrementTakeTwo(number: Int, by: Int) -> Int {
    return number + by
}

print(increment(1, by: 10))           // 11
```

Parameter values aren't modifiable by default.	
 
But you can declare that you want to change a parameter value (within the function) with a var keyword.

```swift
func addOne(var num: Int) -> Int {
    num++
    return num
}
addOne(1)                             // 2
```

### Default parameter values

Swift realizes that the verbosity is becoming a burden.	
 
So default params' internal names are also auto-exposed	with the same external name.

```swift
func incrementTakeThree(number: Int, by: Int = 3) -> Int {
    return number + by
}

print(incrementTakeThree(1))         // 4
print(incrementTakeThree(1, by:1))   // 2
```

### Variadic parameters

(Aside: functions can be overloaded since they're different types [more below])

```swift
func sum(nums: Double...) -> Double {
    var sum: Double = 0.0
    for num in nums {
        sum += num
    }
    return sum
}
```

Unfortunately, splats aren't supported	

So you can't call a variadic function with an array of args.

```swift
func average(nums: Double...) -> Double {
    return sum(nums) / Double(nums.count)
}
average(1.0, 2.0, 3.0, 4.0)            // 2.5
```

### In-Out (pass-by-reference) parameters

inout params cannot be declared as var or let.

```swift
func addOneSideEffect(inout num: Int) {
    num++
}
```

Cannot pass constants and literals.	

```swift
var num = 1
```

Use an & before the var's name.

```swift
addOneSideEffect(&num)
print(num)                         // 2
```

### Function Types

Assign functions to variables.	
 
The function type (signature) of the function must be repeated.

```swift
var mean: (Double...) -> Double = average
mean(1.0, 4.0)                      // 2.5
```

The variable can be re-assigned to another function as long as that function has the same type.	
 
Function types can be used as parameters.

```swift
func skewedMean(mean: (Double...) -> Double, num1: Double, num2: Double) -> Double {
    return mean(num1, num2) + 1.0
}
skewedMean(mean, num1: 1.0, num2: 4.0)          // 3.5
```

Function types can be returned from functions.

```swift
func choose(which: String) -> (Double...) -> Double {
    if which == "sum" {
        return sum
    }
    return mean
}
choose("sum")(1.0, 2.0)            // 3.0
choose("mean")(1.0, 2.0)           // 1.5
```

### Nested Functions

Nested functions cannot be referenced outside of their parent function.

```swift
func summer(var num: Double) -> (Double...) -> Double {
    // Closure: the nested function is able to close over `num`.
    func internalFunc (numbers: Double...) -> Double {
        for i in numbers {
            num += i
        }
        return num
    }

    return internalFunc
}

summer(1.0)(2.0, 3.0)              // 6.0
```

## Closures

```swift
import Foundation
```

Closures have the syntax of:

```swift
/*
{ (parameters) -> return type in
body
}
*/

var numbers = [1, 2, 3, 4]

var reversed = numbers.sort({(a: Int, b: Int) -> Bool in
    return a > b;
})
```

But type inference allows us to omit the types and write something more succinct...

```swift
reversed = numbers.sort({ a, b in return a > b })
```

...And single-expression closures can even omit the return keyword.	

```swift
reversed = numbers.sort({ a, b in a > b })
```

...And even the param names can be omitted.	

```swift
reversed = numbers.sort({ $0 > $1 })
```

...But why stop there? Greater-than is an operator function!	

```swift
reversed = numbers.sort(>)
```

Closures, of course, have the ability to access the variables in scope at the time the function is defined.

```swift
func sizeOfNumbers() -> Int {
    return numbers.count
}

print(String(sizeOfNumbers()))              // 4

numbers.append(5)

print(String(sizeOfNumbers()))              // 5
```

### Trailing Closures

Similar to Ruby's blocks, if a function accepts a closure, you can specify it after the function's parens.	

```swift
reversed = numbers.sort { $0 >  $1 }

var toStringed = numbers.map {
    (num: Int) -> String in
    return String(num)
}
```

## Enums

```swift
enum Example {
    case A
    case B
    case C
    case D
}
```

Unlike enums in other languages, the named labels do not implicitly map to 0, 1.. etc. enum members	are their own values of the type specified by the enum's name.	

```swift
var example = Example.A          // (Enum Value)
```

Once you assign to an enum value, you can reassign to another value without respecifying the the enum name.	

```swift
example = .B
```

Switch statements must be exhaustive or declare	a default case.	

```swift
switch example {
case .A:
    print("A")
case .B:
    print("B")                               // B
case .C:
    print("C")
case .D:
    print("D")
}
```
Enumerations can store values of any type, and type values can be different for every enum member.	

```swift
enum Types {
    case Str(String)
    case Num(Double)
}
```

A variable can be reassigned a different type of the enum.

```swift
var a = Types.Str("hello")
a = .Num(1.0)
```

Associated values can be extracted as part of a switch.	

```swift
switch a {
case .Str(let val):
    print(val)
case .Num(let val):
    print(val)                             // 1.0
}
```

### Raw Values

Enums can prepopulate with "raw" values, similar to other languages.	

```swift
enum Letters: Character {
    case a = "A"
    case b = "B"
    case c = "C"
}
```

When integers are used for raw values, they	auto-increment if no value is specified.	

```swift
enum Numbers: Int {
    case One = 1, Two, Three, Four, Five
}
```

Access raw values with `toRaw`	

```swift
var five = Numbers.Five
print(five.rawValue)                      // 5
```

`fromRaw` tries to find an enum member with a raw value.	
 
An optional is returned.	

```swift
var possibleNum = Numbers(rawValue: 2)!
print(possibleNum == Numbers.Two)       // true
```

TK - type methods and mutating methods	

## Classes

```swift
import Foundation

class Example {
    var a = 0
    var b: String

    init(a: Int) { // Constructor
        self.a = a
        b = "name"                  // An error if a declared property isn't initialized
    }
}
```

External param names are required...	

```swift
let eg = Example(a: 1)
print(eg.a)              // 1
```

...Unless the params are declared with leading underscores.	

```swift
class Example2 {
    var a = 0
    var b = 0

    init(_ a: Int, _ b: Int) {
        self.a = a
        self.b = b
    }
}

let eg2 = Example2(1, 2)
print(eg2.a)            // 1
print(eg2.b)            // 2
```

### Lazy properties

Lazy properties' initial value aren't initialized until the first time the property is accessed.	

```swift
class Podcast {
    lazy var episode = Episode() // `var` declaration is required.
}

class Episode {
    var audio = "somefile.mp3"
}

var podcast = Podcast()          // episode has not been initialized yet.
print(podcast.episode.audio)   // somefile.mp3
```

### Computed properties

Computed properties don't store a value. Instead, getters / setters	are provided to retrieve and set _other_ properties.	

```swift
class Window {
    var x = 0.0, y = 0.0
    var width = 100.0, height = 100.0

    var center: (Double, Double) {
        get {
            return (width / 2, height / 2)
        }

        set(newVal) {
            x = newVal.0 - (width / 2)
            y = newVal.1 - (height / 2)
        }
    }
}

var win = Window()
print(win.center)               // (50.0, 50.0)
win.center = (0.0, 10.0)
print(win.x)                    // -50.0
print(win.y)                    // -40.0
```

The param to `set` can be omitted and a magic "newValue" can be used to referenced the new value.	

```swift
/*
set {
x = newValue.0 - (width / 2)
}
*/
```

### Read-only computed properties

```swift
class Song {
    var title = ""
    var duration = 0.0
    var metaInfo: [String:String] {
        return [
            "title": self.title,
            "duration": NSString(format: "%.2f", self.duration) as String,
        ]
    }
}

var song = Song()
song.title = "Rootshine Revival"
song.duration = 2.01
print(song.metaInfo["title"]!)    // Rootshine Revival
print(song.metaInfo["duration"]!) // 2.01
```

### Property Observers

Property observers can be added onto any properties	(including inherited) except for lazy computed props.	

```swift
class Website {
    var visitors: Int = 0 {             // An explicit type is required
        willSet(newVisitorCount) {          // Called before the prop is set
            visitors = newVisitorCount + 1  // Warning. Can't set within its own willSet
        }
        didSet {                            // Called after a new val is set
            print(visitors - oldValue)    // oldValue is magically defined
        }
    }
}

var site = Website()
site.visitors = 1
print(site.visitors)                 // 1
```

### Type Properties

AKA class variables	

```swift
class Body {
    /*    class var age = 0                 // error: class variables not yet supported */
    // Computed type property
    class var size: Int {
        return 10
    }
}
print(Body.size)                     // 10
```

### Type Methods

AKA class methods	

```swift
class Banana {
    var color = "green"
    class func genus() -> String {
        return "Musa"
    }
}
print(Banana.genus())                // Musa
```

### Instance methods

```swift
class Month {
    var name: String

    init(name: String) {
        self.name = name
    }

    func shortened() -> String {
        return name[name.startIndex..<advance(name.startIndex, 3)]
    }
}
print(Month(name: "January").shortened())    // Jan
```

### Inheritance

Swift classes do not inherit from a universal base class.	

```swift
class Bicycle {
    var tireWidth: Double
    var topSpeed: Double
    var name: String
    var gears: Int
    // Marking a method/property with `@final` prevents it from being overridden
    final var color = "green"

    init() {
        tireWidth = 30.5
        topSpeed = 10.0
        name = "regular ol' bike"
        gears = 3
    }

    func go(distance: Double) {
        print("Went \(distance) at a top speed of \(topSpeed) in my \(name)")
    }
}

class MountainBike : Bicycle {
    /* var tireWidth = 64.0 // Cannot override property in the declaration */
    override init() {
        super.init()

        tireWidth = 64.0
        name = "mountain bike"
        gears = 12
    }
    // Override parent's methods via `override` keyword
    override func go(distance: Double) {
        super.go(distance)
        print("Did \(distance) on a mountain bike")
    }

    // A getter/setter override can _any_ inherited property.
    override var topSpeed: Double {
        get {
            return super.topSpeed - 4.0
        }
        set {
            super.topSpeed = newValue
        }
    }

    // Property observer
    override var gears: Int {
        didSet {
            print("Gears was changed to \(gears)")
        }
    }
}

var mountainBike = MountainBike()              // Gears was changed to 12
mountainBike.topSpeed = 6.0
print(mountainBike.topSpeed)                 // 2.0
mountainBike.go(12.0)                          // Went 12.0 at a top speed of 10.0 in my mountain bike
```

Did 12.0 on a mountain bike	

### Initializers

 
'Convenience' initializers overload empty initializers that populate the params	in 'designated' initializers.	

```swift
class iOS {
    var version: String

    init(version: String) {
        self.version = version
    }

    convenience init() {
        self.init(version: "8.0.0")
    }
}

var os = iOS()
print(os.version)                          // 8.0.0
```

### ARC and reference cycles

Strong reference cycles happen when two objects hold strong references to each other so that neither can be deallocated (à la memory leaks in garbage collected langs)	
 
Strong references can be resolved by declaring references as `weak` or `unowned`	

Use a weak reference whenever it's valid for the reference to be nil at any point. These are optional types.	

```swift
class Driver {
    weak var car: Car? // Strong reference to car.

    deinit {
        print("Driver deinitialized")
    }
}

class Car {
    weak var driver: Driver? // Weak reference to driver.

    deinit {
        print("Car deinitialized")
    }
}

var driver: Driver?
var car: Car?

driver = Driver()
car = Car()
driver!.car = car
car!.driver = driver

driver = nil               // No more strong references to driver.
car = nil                  // No more strong references to car.
```

Unowned references are like weak references except they always refer to a value, so they're non-nil.	

```swift
class Artist {
    var instrument: Instrument?  // Strong reference to instrument.
}

class Instrument {
    unowned let artist: Artist   // Unowned reference to artist.
    init (artist: Artist) {
        self.artist = artist
    }
}

var artist: Artist?
artist = Artist()
artist!.instrument = Instrument(artist: artist!)

artist = nil        // Both objects are deallocated since there are no more strong references.
```

### Access control

Access control in Swift is very much package-based.	
 
`private`: Can only be accessed from the same source file that it's defined	
 
`internal`: Can be accessed anywhere in the target it's defined	
 
`public`: Accessible anywhere in the target and anywhere its module is imported	
 
Defaults to `internal` if not explicitly declared.	

```swift
internal class Image { // Accessible in the same target
    internal var name : String

    private var mime : String {     // Accessible only in this file. Never settable.
        get {
            return "image/\(name.pathExtension)"
        }
    }

    init(name: String) {
        self.name = name
    }
}
var img = Image(name: "foo.png")

public class Webpage {
    public var title : String
    public var created : NSDate
    private(set) var images : [Image] // Readable within the same target but only writable in this file
    var slug : String {
        return created.description + title
    }

    init(title: String) {
        self.title = title
        self.created = NSDate()
        self.images = []
    }
}

var webPage = Webpage(title: "blog post")
webPage.images.append(Image(name:"panda.gif"))
```

## Structures

Structs are copy by value, can't inherit, but can conform to protocols	

```swift
struct Work {
    var location = ""
    var units = [String]()
    let length: Int

    func last() -> String {
        return units[units.count - 1]
    }
}
```

Structs have handy memberwise initializers.	
 
All properties must be specified.	

```swift
var work = Work(location: "office", units: ["answer phone", "read book"], length: 8)

print(work.location)                // office
print(work.units[0])                // answer phone
print(work.last())                  // read book
```

### Type Properties

AKA static variables	

```swift
struct Play {
    static var duration = 1
    static var name: String { // Read-only computed type property
        return String("named \(duration + 1)")
    }
    static var activity: String {
        get {
        return name + "stargaze"
        }
        set {
            duration = 2
        }
    }
}
print(Play.duration)               // 1
print(Play.name)                   // named 2
print(Play.activity)               // named 2stargaze
```

### Instance methods

```swift
struct Mansion {
    var rooms = 30
    var garageDoors = 5

    // Methods cannot modify value types without
    // declaring so via the `mutating` keyword.
    mutating func addGarage (newVal: Int) {
        garageDoors += newVal
    }
}
var mansion = Mansion()
mansion.addGarage(5)
print(mansion.garageDoors)         // 10
```

### Type Methods

AKA static methods	

```swift
struct Font {
    static var size = 12
    var typeface = ""

    static func pointSize() -> String {
        return "\(size)pt"
    }
}
print(Font.pointSize())            // 12pt
```

## Values and References

### Classes are reference types

```swift
class Job {
    var title = ""
}

var job1 = Job()
var job2 = job1
job1.title = "singer"
print(job2.title)                   // singer
```

Triple equals checks identity.	

```swift
print(job1 === job2)                // true
```

### Dictionaries are copied at the point of assignment

```swift
var band1 = ["bob": "singer", "dan": "guitarist"]
var band2 = band1

band2["bob"] = "drummer"
print(band1["bob"])                // singer
```
### Arrays, Strings, and Dictionaries

Arrays, Strings, and Dictionaries are implemented as structs, so they're copied when they're assigned to a new constant or variable, or when they'e passed to a function / method	

```swift
var bands1 = ["radiohead", "telekinesis", "nada surf"]
var bands2 = bands1
print(bands1 == bands2)           // true

bands1[0] = "the orwells"
```

Now `bands2` is a separate copy.	

```swift
print(bands2[0])                  // radiohead
```

## Type Casting

```swift
class Genre {
    var name: String
    init(name: String) { self.name = name }
}

class Classical : Genre {

}

class Pop : Genre {

}

var collection = [
    Pop(name: "Hard Day's Night"),
    Classical(name: "Canon in D"),
]
```

The type of `collection` is inferred to be `Genre`	

Use `is` to check instance of.	

```swift
for song in collection {
    if song is Classical {
        print("\(song.name) is classical!")
    } else if song is Pop {
        print("\(song.name) is poppy!")
    }
}

/*
Hard Day's Night is poppy!
Canon in D is classical!
*/
```

### Downcasting

`as?` returns an optional (if you aren't sure the cast will succeed)	

`as` throws a runtime error if the cast doesn't succeed	

```swift
for song in collection {
    if let popSong = song as? Pop {
        print("pop song")
    } else if let classicalSong = song as? Classical {
        print("classical song")
    }
}

/*
pop song
classical song
*/
```

### AnyObject

Tons of Cocoa APIs (dicts, arrays, etc.) return `AnyObject`, a generic object wrapper. You have to use `as` to cast an `AnyObject` back to the type you expect.	
 
`AnyObject` doesn't allow primitives--only class-based objects.	

```swift
var randomCollection: [AnyObject] = [
    Pop(name: "Bishop Allen"),
    Classical(name: "Bach"),
]

for item in randomCollection {
    let songGenre = item as! Genre
    print("\(songGenre.self)")       // C11lldb_expr_03Pop (has 1 child) // C11lldb_expr_09Classical
    print("\(songGenre.name)")
}
```

Every item can also be downcasted in the for-loop	

```swift
for item in randomCollection as! [Genre] {
    print("\(item.name)")
}

/*
Bishop Allen
Bach
*/
```

### Any

`Any` allows primitives and class-based objects.	

```swift
var groups = [Any]()
groups.append(1.0)
groups.append(1)
groups.append("string")
groups.append(Pop(name: "Long Winters"))

for item in groups {
    switch item {
    case let anInt as Int:
        print("\(item) is an int")
    case let aDouble as Double:
        print("\(item) is a double")
    case let aString as String:
        print("\(item) is a string")
    case let aGenre as Genre:
        print("\(item) is a Genre")
    default:
        print("dunno")
    }
}

/*
1.0 is a double
1 is an int
string is a string
C11lldb_expr_13Pop (has 1 child) is a Genre
*/
```

## Class Extensions

When you add an extension to an existing type, the extension changes are available on existing instances of that type, even if they were declared before the extension.	

```swift
var someInt = 3

extension Int {
    func isOdd () -> Bool{
        return self % 2 != 0
    }

    func isEven() -> Bool {
        return !isOdd()
    }

    func times(task: (Int)->()) {
        for i in 0..<self {
            task(i)
        }
    }
}

print(someInt.isOdd())                 // true
print(2.isEven())                      // true

2.times({(Int i) in
    print(i)                           // 0, 1
})
```

## Protocols

AKA Interfaces in other languages:	
 
abstract properties and methods that concrete classes implement.	

```swift
import Foundation

protocol Image {
    // Protocol defines only a properties'
    // type and whether it's gettable or settable
    var filename: String { get set }
    var filesize: Double { get }
    var mimetype: String { get }
    var height: Double { get }
    var width: Double { get }

    /*    class var someTypeProperty: Int { get set } */

    func save ()

    mutating func resize(width: Double, height: Double)
}
```

Structs can adopt protocols...	

```swift
struct Gif : Image {
    var filename: String
    var filesize: Double
    var mimetype = "image/gif"
    var height: Double
    var width: Double

    func save() {

    }

    mutating func resize(width: Double, height: Double) {

    }
}
```

...or Classes can adopt protocols...	

```swift
class Png : Image {
    var filename: String
    var filesize: Double
    var mimetype = "image/png"
    var height: Double
    var width: Double

    init(filename: String, filesize: Double, width: Double, height: Double) {
        self.filename = filename
        self.filesize = filesize
        self.width = width
        self.height = height
    }

    func save() {

    }

    // `mutating` is not required when the protocol already
    // declares it as such.
    func resize(width: Double, height: Double) {

    }
}

var gif = Gif(filename: "rainbow.gif", filesize: 2.3, mimetype: "image", height: 50, width: 50)
var png = Png(filename: "carry.png", filesize: 3.4, width: 100, height: 54)
```

Protocols are full-fledged types	

```swift
func resizeImage (var img: Image) {
    img.resize(50.0, height: 50.0)
}
```

Protocols are heavily used as part of the delegate pattern so that classes can call delegates' hook methods during certain lifecycle events.	
 
### Protocol Composition

A type can be composed of multiple protocols.	

```swift
protocol Video {
    var framerate: Int { get }
    var resolution: Double { get }
}


struct Media: Image, Video {
    var filename: String
    var filesize: Double
    var mimetype: String
    var height: Double
    var width: Double
    var framerate: Int
    var resolution: Double

    func save() {

    }

    func resize(width: Double, height: Double) {

    }
}
```

Each protocol is comma-separated within brackets.

```swift
func save(media: protocol<Image, Video>) {
    media.save()
}
```

### Protocol Checking

`is` and `as` work as on any other type.	
 
### Optional Protocol Requirements

Denoted with `@optional`.	
 
This feature requires protocols be marked with `@objc`, which exposes the protocol to Objective-C code.	

```swift
@objc protocol Time {
    var day: Int { get }
    var month: Int { get }
    var year: Int { get }
    optional var hour: Int { get }
    optional var minute: Int { get }
    optional var second: Int { get }

    func toString () -> String
}

class ShortDate : Time {
    @objc var day: Int
    @objc var month: Int
    @objc var year: Int

    init(day: Int, month: Int, year: Int) {
        self.day = day
        self.month = month
        self.year = year
    }

    @objc func toString () -> String {
        return "\(day)/\(month)/\(year)"
    }
}

class LongDate : ShortDate {
    var hour: Int = 0
    var minute: Int = 0
    var second: Int = 0

    convenience init(day: Int, month: Int, year: Int, hour: Int, minute: Int, second: Int) {
        self.init(day: day, month: month, year: year)
        self.hour = hour
        self.minute = minute
        self.second = second
    }

    override func toString() -> String {
        return super.toString() + " \(hour):\(minute):\(second)"
    }
}

var dates: [AnyObject] = [
    ShortDate(day: 5, month: 5, year: 2016),
    LongDate(day: 5, month: 5, year: 2016, hour:10, minute:1, second: 0)
]

for item in dates {
    let date = item as! Time

    print("\(date.toString())")                              // 5/5/2016 5/5/2016 10:1:0

    if let hours = date.hour {
        print("Hour of the day: \(hours)")                  // Hour of the day: 10
    }
}
```

## Generics

```swift
import Foundation
```

Generic functions can operate on any type.	

Type parameters specify a placeholder type in brackets after the function name.	

Thereafter, that placeholder can be used as a type parameter or within the function.	

```swift
func log<ToBeLogged>(a: ToBeLogged) {
    print(a)
}

log("les filles")
log(1)
```

### Generic Types

Generic types are your own structs, classes, and enums that work with any type.	

This is how all of Swift's built-in types work.	

```swift
struct Bucket<Bucketable> {
    var items = [Bucketable]()

    mutating func add(item: Bucketable) {
        items.append(item)
    }

    mutating func remove() {
        items = []
    }
}

var bucket = Bucket<Int>()
bucket.add(1)

var bucket2 = Bucket<String>()
bucket2.add("special")

class Basket<Basketable> {
    var items = [Basketable]()

    func add(item: Basketable) {
        items.append(item)
    }

    func remove() {
        items = []
    }
}

var basket = Basket<Int>()
basket.add(1)

var basket2 = Basket<String>()
basket2.add("control")
```

### Type Constraints

You can ensure that a generic type conforms to a protocol or is a subclass of a specific type.	
 
Without adding the `Equatable` type constraint, the compiler doesn't know whether &#96;==&#96; will work and thus won't compile this func.	

```swift
func equal<T: Equatable>(a: T, b: T) -> Bool {
    return a == b
}
```

### Associated Types

Protocols can define placeholder names (aliases) to types that are used as part of the protocol.	

```swift
protocol Vase {
    typealias Plant
    mutating func add(item: Plant)
    var size: Int { get set }
    mutating func remove() -> Plant
}

class GrecianUrn : Vase {
    typealias Plant = String

    var size = 10
    var plantName = ""

    func add(item: String) {
        plantName = item
    }

    func remove() -> String {
        let name = plantName
        plantName = ""
        return name
    }
}
```

## Where clauses

You can impose more rigorous conformance between types using a `where` clause after the list of type params within the brackets.	

```swift
var a = ["a"]
print(a.dynamicType)
print(object_getClassName(a))

protocol Container {
    typealias Thing
    func size() -> Int
    func add(thing: Thing)
}

class Crate<Thing> : Container {
    var items = [Thing]()

    func size() -> Int {
        return items.count
    }

    func add(thing: Thing) {
        items.append(thing)
    }
}

func similarCrates<C1: Container, C2: Container where C1.Thing == C2.Thing> (crate1: C1, crate2: C2) -> Bool {
    return crate1.size() == crate2.size()
}

var stringCrate = Crate<String>()
stringCrate.add("stickers")

var intCrate = Crate<Int>()
intCrate.add(22)
```

This fails: 'String' is not identical to 'Int'	

```swift
/* similarCrates(stringCrate, intCrate) */

var anotherStringCrate = Crate<String>()
similarCrates(stringCrate, crate2: anotherStringCrate)                 // false
anotherStringCrate.add("goo")
similarCrates(stringCrate, crate2: anotherStringCrate)                 // true
```

## Advanced Operators

```swift
import Foundation
```

### Bitwise Operators

```swift
var a: UInt8 = 0b00001111
var b: UInt8 = 0b11110000
````

`NOT`

```swift
print(~a == b)                   // true
```

`AND`

```swift
print(a & b == 0)                // true
```

`OR`

```swift	
print(a | b == 0b11111111)       // true
```

`XOR`

```swift	
print(a ^ b == a | b)            // true
```

Shifts (unsigned)

```swift
print(a << 1 == 0b00011110)
print(a >> 1 == 0b00000111)
```

### Overflow Operators

Normally Swift will error when and over/underflow occurs.	

```swift
var num = UInt8.max
```

error: arithmetic operation results in an overflow

```swift
/* var zero = num + 1 */
```

But prefixing the operator with "&" tells Swift	that you want this behavior.	

```swift
var zero = num &+ 1
print(zero == 0)                 // true
```

### Operator Functions

Operator overloading is a supported language feature, but it isn't a best practice in most cases.	

```swift
struct Trip {
    var distance = 0.0, duration = 0.0
}
```

Defines an 'infix' + operator that handles two args.	

```swift
func + (left: Trip, right: Trip) -> Trip {
    return Trip(distance: left.distance + right.distance,
        duration: left.duration + right.duration)
}
```

Defines a 'prefix' - operator that handles a single arg.	

```swift
prefix func - (trip: Trip) -> Trip {
    return Trip(distance: trip.distance * -1.0, duration: trip.duration * -1.0)
}
```

Compound assignments are also supported.	

```swift
func += (inout left: Trip, right: Trip) {
    left = left + right
}
```

As well as assignment expressions.	

```swift
prefix func ++ (inout trip: Trip) -> Trip {
    trip += Trip(distance: 1.0, duration: 1.0)
    return trip
}

var tripA = Trip(distance: 100.0, duration: 2.0)
var tripB = Trip(distance: 250.0, duration: 5.0)

var combined = tripA + tripB

print("Went \(combined.distance) in \(combined.duration)")        // Went 350.0 in 7.0

var disaster = -tripA

print("Went \(disaster.distance) in \(disaster.duration)")        // Went -100.0 in -2.0

tripA += tripB
tripA += tripB

print("Went \(tripA.distance) in \(tripA.duration)")        // Went 600.0 in 12.0

++tripA

print("Went \(tripA.distance) in \(tripA.duration)")        // Went 601.0 in 13.0
```

You can also define your own bananas custom operators with / = - + * % < > ! & | ^ . ~	

```swift
postfix operator -=- {}

postfix func -=- (inout trip: Trip) -> Trip {
    trip = Trip(distance: Double(Int(trip.distance) * random()),
        duration: Double(Int(trip.duration) * random()))
    return trip
}

tripA-=-

print("Went \(tripA.distance) in \(tripA.duration)")        // Went 602222301.0 in 1311110111.0
```




## Generators

```swift
import Foundation
```

`GeneratorType` is a type of `SequenceType`.	
 
In order to conform, set the Element typealias and implement the `next` method.

```swift
class Incrementing : GeneratorType {
    typealias Element = Int
    var counter = 0

    func next() -> Element? {
        return counter++
    }
}

var count = Incrementing()
count.next()!                         // 0
count.next()!                         // 1
```

`for` loops work with any `SequenceType` type.	
 
A `SequenceType` has a `generate` method that returns a `GeneratorType`.	

```swift
class Fibonacci : SequenceType {
    func generate() -> GeneratorOf<Int> {
        var current = 0
        var next = 1

        return GeneratorOf<Int> {
            var returnVal = current
            current = next
            next += returnVal

            return returnVal
        }
    }
}

var fibSequence = Fibonacci()

for i in fibSequence {
    if i > 50 {
        break
    }

    print("\(i) ")                // 0 1 1 2 3 5 8 13 21 34
}
```
