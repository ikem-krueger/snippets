import UIKit
import GameController

class ViewController: UIViewController {
	@IBOutlet weak var textField: UITextField!
	
	var gameController: GameController!

	override func viewDidLoad()	{
		super.viewDidLoad()
		
		gameController = GameController()
		gameController.delegate = self // <-- Here is the delegate
	}

	@IBAction func touchButton(sender: AnyObject) {
		gameController.updateText()
	}
}