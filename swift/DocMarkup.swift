/// Some introductory test that describes the purpose
/// of the function.
func someFunction(name: String) -> Bool {
  return false
}

/**
 What does this function do?
*/
func anotherFunction(name: String) -> Bool {
  return false
}

/**
 Another useful function
 - parameters:
   - alpha: Describe the alpha param
   - beta: Describe the beta param
*/
func doSomething(alpha: String, beta: String) {
    return true
}

/**
 Another useful function
 - parameter alpha: Describe the alpha param
 - parameter beta: Describe the beta param
*/
func doSomethingElse(alpha: String, beta: String) {
    return nil
}
