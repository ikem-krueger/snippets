Assuming you have downloaded the `.dsc file`, the `.orig.*z` file, and maybe also a `.debian.*z` file, then unpack the source package with:

```
dpkg-source -x [package_name]_[package_version].dsc
```

Move into the extracted source folder and run:

```
sudo mk-build-deps -i
```

This will create the file `[package_name]-build-deps_[version].deb` and install it, and also install all the dependencies it suggests.

Now you should be able to build your package from source:

```
dpkg-buildpackage -us -uc
```

When you are finished, you can easily remove the dependencies that were pulled in, by removing the build-deps package which you generated:

```
sudo apt-get purge --auto-remove [package_name]-build-deps
```
