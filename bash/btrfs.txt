btrfs fi show
btrfs device stats [path to filesystem or disk]
btrfs filesystem df $PATH  
btrfs filesystem show  
btrfs filesystem resize $DEVICE_ID [+/-] $NEW_SIZE $FILESYSTEM  
btrfs filesystem resize $DEVICE_ID max $FILESYSTEM  
btrfs balance start
btrfs balance start [btrfs mount point]  
btrfs balance status /path/to/mount  
btrfs filesystem defragment [btrfs filesystem path]  
btrfs check --force /dev/mmcblk0p2
btrfs scrub start /path/to/filesystem/mount  
btrfs scrub status /path/to/filesystem/mount  
btrfs device add -f /dev/sd[x] [btrfs mount point]  
btrfs device delete /dev/sd[x] [btrfs mount point]
btrfs device delete missing [btrfs mount point]
btrfs subvolume create  
btrfs subvolume list  
btrfs subvolume snapshot  
btrfs subvolume delete  
btrfs property get -ts /path/to/subvolume
btrfs property set -ts /path/to/subvolume ro false
btrfs subvolume delete /mnt/sdc2/home/\@*-ro
mount -t btrfs -o subvol=@root /dev/sda2 /mnt
mount -t btrfs -o subvolid=5 /dev/mmcblk0p2 /mnt/mmcblk0p2
btrfs send /mnt/mmcblk0p2/home/\@btrfs-auto-snap_daily-2021-02-13-0941-ro|pv|btrfs receive -vv /mnt/sdc2/home
btrfs send -f outfile
btrfs receive -f outfile
