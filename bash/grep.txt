grep . file.txt # remove blank lines (not including lines with spaces)
grep "\S" file.txt # remove blank lines (including lines with spaces)
grep -m1 "" file.txt # print the first line
grep -o '[^,]*$'
grep -o <pattern> # output the matching regex pattern in a line
