#!/bin/bash

init () {
if [ ! -d /sys/class/gpio/gpio${PIN} ]
then
  echo "$PIN" > /sys/class/gpio/export
  echo "out" > /sys/class/gpio/gpio${PIN}/direction
fi
}

get_value () {
cat /sys/class/gpio/gpio${PIN}/value
}

set_value () {
echo "$VALUE"|sudo tee /sys/class/gpio/gpio${PIN}/value
}

case $# in
  1)
    PIN=$1

    init
    get_value
    ;;
  2)
    PIN=$1
    VALUE=$2

    init
    set_value
    ;;
  *)
    echo "Usage: gpio.sh <PIN> [VALUE]"
esac
