tvservice -s # get display state
tvservice -e "DMT 87" # set display state to ...
tvservice -m DMT # get modes for analog/dvi displays
tvservice -m CEA # get modes for hdmi displays
tvservice --off
tvservice -c "PAL 16:9"
fbset -depth 8; fbset -depth 16; # reset the framebuffer
fbset -g 1280 720 1280 720 16 # set framebuffer size to "hd ready"
