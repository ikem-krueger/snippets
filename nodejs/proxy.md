# Node.js behind a proxy 

## Write proxy settings

```
$ npm config set proxy http://localhost:3128
$ npm config set https-proxy http://localhost:3128
```

This config is great for [SquidMan](http://squidman.net/) app.

## Delete proxy settings

```
$ npm config delete http-proxy
$ npm config delete https-proxy
```
