PsExec.exe -s -i cmd.exe # run cmd.exe as user "nt authority\system"
del /r /s "C:\ProgramData\Microsoft\Windows Defender\Scans\History\Service" # delete windows defender scan history
zip -T filename.zip # test zip file integrity
unzip -t filename.zip # test zip file integrity
pip list -v # list python packages install locations
pyinstaller --onefile -w main.py # convert .py file to .exe
jabswitch -enable ; enable java access bridge (used by blind people)
jabswitch -disable ;
rmdir c:\test /s ; remove the directory "C:\test" with prompting
rmdir c:\test /s /q ; remove the directory "C:\test" without prompting
mbr2gpt /validate /allowFullOS ; convert mbr to gpt partition style
msiexec /a c:\file.msi /qb TARGETDIR=c:\temp ; extract files from an msi package
taskkill /f /t /im wwahost.exe ; kill a program by process name
icacls .\private.key /inheritance:r ; fix permissions for 'private-key'
icacls .\private.key /grant:r "%username%":"(R)" ; fix permissions for 'private-key'
wsl -l -v ; check wsl version
netstat -aon ; view port use along with process identifiers
icacls "Path\to\folder" /save "C:\data\permissions.txt" /t /c ; backup the permissions for the specified folder
icacls "Path\to\parent\folder" /restore "C:\data\permissions.txt" ; restore the permissions for a folder
icacls "full path to the folder" /reset /t /c /l ; reset permissions for a folder, its files, and subfolders
pendmoves ; show pending rename/delete
movefile C:\source C:\dest ; schedule move on next reboot
movefile test.exe "" ; schedule delete on next reboot
type .ssh\id_ecdsa.pub | ssh user@host "cat >> .ssh/authorized_keys" ; transfer public key to server
mklink /D TestLink \Test
q -H -d ";" "SELECT * FROM file.csv WHERE Column LIKE '%Something%'" ; run sqlite queries on csv files
java -cp junit-4.12.jar;hamcrest-core-1.3.jar;. org.junit.runner.JUnitCore UserDAOTest ProductDAOTest
javac -cp junit-4.12.jar;. UserDAOTest.java ProductDAOTest.java
javac -J-Dfile.encoding=UTF-8 File.java
powershell -ExecutionPolicy ByPass -File Script.ps1
echo %CD% ; get current directory
qemu-img.exe convert -f vhdx Nano2016.vhdx -O vmdk -o subformat=streamOptimized Nano2016.vmdk -p
jar cvfe myArchiv.jar MyMainClass -C myJar . # create an executable jar file
route ADD 157.0.0.0 MASK 255.0.0.0  157.55.80.1 METRIC 3 IF 2
ps2pdf -dPDFSETTINGS#/screen tmp.ps output.pdf ; the use of "#" instead of "=" is necessary for every parameter/value pair
whoami /user ; show user including their security identifier
icacls file /grant Administrator:F ; grant full access to file
start /b dotnet run ; run a command on the background
set ; set, clear or shows environment variables for cmd.exe
setx ; creates or modifies environment variables in the user or system environment
mysql -u root ; xampp mysql login
timeout /t 10 ; timeout/sleep for 10 seconds
arp -a ; show all devices in the network
dd if=\\?\Device\Harddisk3\Partition0 of=xbian-new.img bs=512 count=15554561 --progress
cipher /e C:\Users\TechSpot\Desktop\Folder ; encrypt a file or folder
cipher /d C:\Users\TechSpot\Desktop\Folder ; decrypt that file or folder
compact /c /s /i ; compress all the files in the current directory recursively
compact /c /s:C:\Users\TechSpot\Desktop\Folder ; compress a file or folder
compact /u /s:C:\Users\TechSpot\Desktop\Folder ; decompress a file or folder
driverquery /FO list /v
driverquery > output.txt
where find ; show path of find
gdisk \\.\physicaldrive0
gdisk 0:
devcon.exe remove =USB
devcon.exe rescan =USB
%Windir%\System32\Printing_Admin_Scripts\de-DE\prnmngr.vbs -l ; list printer
%Windir%\System32\Printing_Admin_Scripts\de-DE\prnmngr.vbs -d -p "printer" ; delete printer
%Windir%\System32\Printing_Admin_Scripts\de-DE\prncnfg.vbs -x -p OldPrinter45 -z NewPrinter64 ; rename printer 
java -p "C:\Program Files\Java\javafx-sdk-11.0.2\lib" --add-modules javafx.swing --add-modules javafx.fxml --add-modules javafx.controls -jar launch4j.jar
java -p "C:\Program Files\Java\javafx-sdk-11.0.2\lib" --add-modules javafx.swing --add-modules javafx.fxml --add-modules javafx.controls -jar FileBot.jar "$@"
sc config CDPUserSvc start= disabled
jshell ; interactive java shell
windd64 /if:\\.\PhysicalDrive2 /of:.\image.raw
mountvol ; show possible mount volumes
mountvol e: \\?\Volume{557602f2-042b-4139-8ee2-33ca58a10e52}\ ; mount volume to drive letter e:
mountvol e: /d ; remove drive letter e:
netsh wlan export profile folder=C:\ key=clear
rsync -a --progress --files-from=Filme.txt /cygdrive/z/Daten/Videos/Filme .
systemreset
systemreset -cleanpc
g++ hello.cpp -o hello
%APPDATA%\Gimp\2.10\plug-ins
%APPDATA%\Mozilla\Firefox\Profiles\%PROFILE%\prefs.js ; custom made settings
7z.exe a -tzip archive.zip *
atiflash -p -0 -newbios rv670_mod.bin -f
bcdboot c:\windows /s c: ; restore boot manager
bootrec /fixboot
bootrec /fixmbr
bootrec /rebuildbcd
bootsect /nt60 SYS
C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup
C:\Users\User\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
C:\Windows\System32\drivers\etc ; hosts file location in Windows
chcp 1252 ; set cmd codepage to 1252
chkdsk e: /f ; fix disk (3 stages)
chkdsk e: /r ; repair disk (5 stages)
chkntfs /x c: ; cancel scheduled disk check
cleanmgr /sagerun:65535
cleanmgr /sageset:65535
compact /c /s /a /f /q /i /exe:xpress16k %PROGRAMFILES%
control /name Microsoft.NetworkAndSharingCenter ; start network and sharing center
convert d: /fs:ntfs /NoSecurity ; convert an usb-stick from fat32 to ntfs
copy d:\windows\system32\config\RegBack\* d:\windows\system32\config ; restore your windows registry
csc file.cs /reference:System.IO.Compression.FileSystem.dll ; add ZipFile assembly via path
dd --list
dd if=\\?\Device\Harddisk0\Partition0 of=hackintosh.img bs=4M count=1500 --progress
devcon disable =Net USB\* ; disable usb network device
devcon enable =Net USB\* ; enable usb network device
devenv /upgrade xxx.sln ; upgrade visual studio project file
devenv /upgrade xxx.vcproj ; upgrade visual studio project file
Dir|Rename-Item âNewName { $_.name âreplace " ","_" } ; replace each space character with an underscore
dmg2img mac.dmg maciso.iso
dnscmd /clearcache ; clear the local cache
elevate %CMD% ; run command with elevated privileges
expand -d file.cab ; show filelist in source
finddupe -hardlink c:\photos ; replace duplicates with hard links
findstr /c:"[SR]" %windir%\Logs\CBS\CBS.log >"%userprofile%\Desktop\sfcdetails.txt" 
findstr /c:"[SR]" CBS.log >sfcdetails.txt
firefox -private ; start firefox in the "privacy mode"
for %%a in (*.txt) do ren "%%~fa" "%%~na1.*"
for %f in (.\*) do @echo %f ; iterate through files in current dir
for %file in (file1 file2 file3) do (xcopy /h %file <destination>) ; copy hidden files to destination
for /d %s in (.\*) do @echo %s ; iterate through subdirs in current dir
for /r %f in (.\*) do @echo %f ; iterate through files in current and all subdirs
gcc -c -mno-cygwin hello.c && c++ -o hello.exe -mno-cygwin hello.o -I/usr/local/mingw/include -L/usr/local/mingw/lib
icacls "C:\Users" /reset /t
icacls "C:\Users" /t /grant:r "Administrator":(OI)(CI)F
icacls . /t /c /reset
imdisk -a -s 536870912 -m Z: -o rem -p "fs:ntfs /q /y" ; create a virtual usb-stick with 512MB size
ipconfig /all | findstr /R "DNS\ Servers"
ipconfig /all|findstr /R "DNS\ Servers" ; show dns server address
ipconfig /flushdns ; clear the local cache
mbr2gpt /convert ; convert the drive from mbr to gpt
mbr2gpt /validate ; validate that the drive meets the requirements
mklink /d "C:\Users\Default\Downloads" "D:\Downloads" ; create symbolic link under Windows
msdt /id <diagnostic_id> ; run a troubleshooter from the command line
msdt /id NetworkDiagnosticsWeb
msiexec /a c:\testfile.msi /qb TARGETDIR=c:\temp\test
netdiag /fix
netdiag /v /l /test:dns ; dns test with verbose output saved to netdiag.log
netplwiz ; advanced user account management
nvflash -4 -5 -6 9600OC.rom
perfmon /res ; monitor network activity
pnputil -d oem1.inf ; delete driver
pnputil -e # show all available drivers
reagentc /disable
reagentc /disable ; disable windows recovery environment
reagentc /enable ; enable windows recovery environment
reagentc /info ; check winre installation
reg import yourfile.reg
reg load hklm\Old_System C:\Windows\System32\Config\System
reg load hku\Old_User C:\Users\Jack\ntuser.dat
reg unload hklm\Old_System
regedit /s yourfile.reg
rmdir D:\Temp ; delete empty directory
robocopy C:\Windows D:\Windows /mir ; backup windows folder
rstrui /OFFLINE:c:\windows ; system restore on another windows installation
runas /noprofile /user:%USER% "%CMD%" ; run command as user
runas /user:Administrator regedit
rundll32 shell32.dll,Control_RunDLL hotplug.dll ; open safe hardware remove dialog
schtasks /Create /RU SYSTEM /TN "Aero Glass" /XML task.xml
schtasks /Query
schtasks /Run /TN "Aero Glass"
set /p age=Please input age:
sfc /scanfile=d:\windows\system32\kernel32.dll
sfc /scannow /offbootdir=d:\ /offwindir=d:\windows
shell:Common Startup ;  open the startup folder for all user accounts
shell:Startup ; open the startup folder for your account
slmgr.vbs /dli ; check your windows 8 version
start <file> ; open file with default program
start <program> <file> ; open file with program
start cmd ; create a new cmd.exe window
takeown /r /f .
taskkill /im explorer.exe /f
taskmgr /0 /startup ; open the task manager startup tab
tskill explorer
vboxmanage modifyhd HardDisks/Windows_XP.vdi --type immutable
vboxmanage modifyhd Machines/Windows_XP_VPN/Snapshots/<image> --autoreset false
vboxmanage setextradata <vmname> "VBoxInternal/PDM/HaltOnReset" 1
vcupgrade /overwrite xxx.vcproj ; upgrade visual studio project file
winsat disk -seq -read -drive c ; measure hard disk read speed for drive c:
winsat disk -seq -write -drive c ; measure hard disk write speed for drive c:
wmiofck -hwmidevs.h -m -u moftable.mbf ; decompile moftable.mbf to hwmidevs.h
wuauclt.exe /updatenow ; trigger windows update
wusa /uninstall /kb:{update ID} ; uninstall windows update
wusa /uninstall /kb:3035583 ; uninstall windows 10 upgrade update
