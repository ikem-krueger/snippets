%d – signed decimal integer
%f – floating point number
%o – octal number
%c – character
%s – string
%i – integer base 10
%u – unsigned decimal number
%x – hexadecimal number
%% – % (percentage)
%n – new line = \n
