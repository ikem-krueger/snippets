/* Program: ip2morse.c
 * Platform/OS: RaspberryPi/Raspbian
 * Description: The purpose of this program is to output the IPv4 
 * address of a network interface on a LED in Morse 
 * code.
 *
 * Copyright (c) 2017 by Andre M. Maier - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the Creative Commons Attribution 4.0 International 
 * (CC BY 4.0) license. For further information, please visit 
 * https://creativecommons.org/licenses/by/4.0
 */
#include <stdio.h>
#include <wiringPi.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#define PIN 1 // BCM GPIO 18
#define SHORT_IN_MS 200 // Length of a dot in Morse code
#define LONG_IN_MS 600 // Length of a dash in Morse code
#define PAUSE_IN_MS 200 // Pause within dots and dashes

// Pause between digits; The pause between the octets of the IP
// address will be twice this value.
#define PAUSE_AFTER_DIGIT_IN_MS 1000

// Network interface for which to get the IP address
#define INTERFACE "wlan0"

// Digits in Morse code (from 0 to 9)
const char* const numbers_in_morse[] = { "-----", ".----", "..---",
                                         "...--", "....-", ".....",
                                         "-....", "--...", "---..",
                                         "----." };

/* Outputs a specified decimal digit (0-9)
   on the GPIO pin. */
void digit_to_morse_output( unsigned char digit ) 
{
   const char* num_in_morse = numbers_in_morse[ digit ];
   int i = 0;
   while( num_in_morse[ i ] != '\0' )
   {
      digitalWrite( PIN, 1 );
      if( num_in_morse[ i ] == '.' ) 
      {
         delay( SHORT_IN_MS );
      }
      else
      {
         delay( LONG_IN_MS );
      }
      digitalWrite( PIN, 0 );
      delay( PAUSE_IN_MS );
      i++;
   }
}

/* Determines the IPv4 address from the specified network
   interface and returns it as a string in dotted-decimal
   format. */
char* get_ip_address_string( char* interface )
{
   struct ifreq ifr;
   int fd = socket( AF_INET, SOCK_DGRAM, 0 );
   ifr.ifr_addr.sa_family = AF_INET;
   strncpy( ifr.ifr_name, interface, IFNAMSIZ - 1 );
   ioctl( fd, SIOCGIFADDR, &ifr );
   close( fd );
   return inet_ntoa( ( ( struct sockaddr_in *)&ifr.ifr_addr ) 
                     -> sin_addr );
}
 
/* Main function. This is where the program starts. */
int main( int argc, char** argv )
{
   // Setting up wiringPi and initializing the GPIO port
   if( wiringPiSetup() == -1 )
     return 1;
   pinMode( PIN, OUTPUT );
 
   // Determine ip address of the network interface
   char* ip_address = get_ip_address_string( INTERFACE );

   // Looping through the whole IPv4 string. The dots between
   // the octets will be represented by a long pause (twice
   // as long as the pause between the digits.
   int length = strlen( ip_address );
   int i;
   for( i = 0; i < length; i++ )
   {
      char c = ip_address[ i ];
      if( c == '.' )
      {
         delay( PAUSE_AFTER_DIGIT_IN_MS );
      }
      else
      {
         unsigned char digit = c - '0';
         digit_to_morse_output( digit );
         delay( PAUSE_AFTER_DIGIT_IN_MS );
      }
   }
   return 0;
}