echo "Fix WMI..."

net stop winmgmt /y

if exist %windir%\system32\wbem\repository.001 rmdir /s /q %windir%\system32\wbem\repository.001

rename %windir%\system32\wbem\repository repository.001

%windir%\system32\wbem\winmgmt /clearadap
%windir%\system32\wbem\winmgmt /kill
%windir%\system32\wbem\winmgmt /unregserver
%windir%\system32\wbem\winmgmt /reserver
%windir%\system32\wbem\winmgmt /resyncperf

regsvr32 /s %systemroot%\system32\scecli.dll
regsvr32 /s %systemroot%\system32\userenv.dll

mofcomp %windir%\system32\wbem\cimwin32.mof
mofcomp %windir%\system32\wbem\cimwin32.mfl
mofcomp %windir%\system32\wbem\rsop.mof
mofcomp %windir%\system32\wbem\rsop.mfl

cd \windows\system32\wbem

for /f %%s in ('dir /b /s %windir%\system32\wbem\*.dll') do regsvr32 /s %%s
for /f %%s in ('dir /b /s %windir%\system32\wbem\*.mof') do mofcomp %%s
for /f %%s in ('dir /b %windir%\system32\wbem\*.mfl') do mofcomp %%s

net start winmgmt

%windir%\system32\wbem\wmiprvse /regserver
%windir%\system32\wbem\winmgmt /regserver

