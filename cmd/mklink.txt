mklink link target ; creates a soft link "link" which points to file "target"
mklink /D link target ; creates a soft link "link" which points to directory "target"
mklink /H link target ; creates a hard link "link" which points to file "target"
mklink /J link target ; creates a hard link "link" which points to directory "target"
