set "LocalAccount=MyUser"

for /f "tokens=*" %%i in ('"wmic useraccount where name=^'%LocalAccount%^' get sid | findstr S-1-5"') do set SidValue=%%i

net user %LocalAccount% /del
del C:\Users\%LocalAccount% /F /S /Q
rd C:\Users\%LocalAccount% /S /Q
reg delete "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\%SidValue%" /f
