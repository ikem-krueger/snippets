Attribute VB_Name = "Modul1"
'' Add progress bar only to all non-hidden pages
Sub AddProgressBar()
    On Error Resume Next
        With ActivePresentation
              sHeight = 0
              n = 0
              j = 0
              For i = 1 To .Slides.Count
                If .Slides(i).SlideShowTransition.Hidden Then j = j + 1
              Next i:
              For i = 0 To .Slides.Count
                .Slides(i).Shapes("progressBar").Delete
                If .Slides(i).SlideShowTransition.Hidden = msoFalse Then
                   Set slider = .Slides(i).Shapes.AddShape(msoShapeRectangle, 0, sHeight, (i - n) * .PageSetup.SlideWidth / (.Slides.Count - j), 4)
                   With slider
                       .Fill.ForeColor.RGB = RGB(213, 19, 23)
                       .Line.ForeColor.RGB = RGB(213, 19, 23)
                       .Name = "progressBar"
                   End With
                Else
                   n = n + 1
                End If
              Next i:
        End With
End Sub

'' Macro to remove the progress bar from all the slides
Sub RemoveProgressBar()
    On Error Resume Next
        With ActivePresentation
              For i = 1 To .Slides.Count
              .Slides(i).Shapes("progressBar").Delete
              Next i:
        End With
End Sub
