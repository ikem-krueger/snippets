| Framework | [Analog](https://analogjs.org/) | [Astro](https://astro.build/) | [Next.js](https://nextjs.org/) | [Nuxt](https://nuxt.com/) | [Qwik](https://qwik.dev/) | [Remix](https://remix.run/) | [SolidStart](https://start.solidjs.com/) | [SvelteKit](https://kit.svelte.dev/) |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Lizenz | MIT | MIT | MIT | MIT | MIT | MIT | MIT | MIT |
| erster Commit | 06.07.2022 | 15.03.2021 | 05.10.2016 | 02.07.2020 | 13.02.2021 | 09.07.2020 | 01.04.2021 | 15.10.2020 |
| Weekly NPM-Downloads | 767 | 186k | 5M  | 585k | 13k | 192k | 6k  | 209k |
| Clientframework | Angular | –   | React | Vue | –   | React | SolidJS | Svelte |
| Rendering-Modus |     |     |     |     |     |     |     |     |
| clientseitiges Rendering (CSR) | ✓   | ✓   | ✓   | ✓   | –   | ✓   | ✓   | ✓   |
| serverseitiges Rendering (SSR) | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| Static Site Generation (SSG) | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| Streaming | –   | ✓   | ✓   | –   | –   | ✓   | ✓   | ✓   |
| Routing |     |     |     |     |     |     |     |     |
| filebasiert | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| tiefe Hierarchie | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| API-Routen | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| Deployment und Einsatz |     |     |     |     |     |     |     |     |
| self-hosted (Node.js) | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| Anbieter wie Netlify oder Vercel | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| statisches Hosting | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   | ✓   |
| bevorzugter Einsatzzweck | Prototypen, kleinere CMS-basierte Webseiten | Produktpräsentationen, E-Commerce, Unternehmenswebseiten | Applikationen mittlerer bis hoher Interaktivität, E-Commerce, CMS, Landingpages und Marketingwebseiten | E-Commerce-Plattformen, CMS-basierte Webseiten, Landingpages und Marketingwebseiten | Portfolioseiten, Landingpages, geräteübergreifende Applikationen | formularintensive Anwendungen wie etwa CMS, auch für Unternehmenswebseiten oder Informationsportale | statische Webseiten, Blogs, Prototyping, Unternehmenspräsentationen | Unternehmenswebseiten, E-Commerce, Prototyping |
| Besonderheiten | Nx-Integration | frontendunabhängig, Inselarchitektur | zusätzliche Optimierungen wie Fonts und Bilder, gute Integration in Vercel, sehr modern | sehr gute Integration in das Vue-Ökosystem, Optimierungen für Assets | Resumability, feingranulares Lazy Loading, wenig clientseitiges JavaScript | React-Router-Integration, Nested Routes, Loaders- und Actions-Architektur, Fokus auf Stabilität | reaktives System | leichtgewichtig, Fokus auf Performance, offizielle Ergänzung für Svelte |
