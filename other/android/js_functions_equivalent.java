// setInterval()
new Timer().scheduleAtFixedRate(new TimerTask(){
    @Override
    public void run(){
       Log.i("interval", "This function is called every 5 seconds.");
    }
},0,5000);

// setTimeout()
new android.os.Handler().postDelayed(
    new Runnable() {
        public void run() {
            Log.i("timeout","This function is called after 5 seconds.");
        }
}, 5000);