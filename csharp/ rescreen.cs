/* tiny tool to move all Windows to the main Screen
 * https://tcpip.wtf/en/rescreen-tool-to-move-all-windows-to-your-win-main-screen.htm
 */
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace rescreen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("rescreen - tiny window position mover.");
            Console.WriteLine("more about me at: https://tcpip.wtf");
            Process[] processlist = Process.GetProcesses();
            foreach (Process process in processlist)
            {
                // only move Windows with a set title
                if (!String.IsNullOrEmpty(process.MainWindowTitle) && !String.IsNullOrEmpty(process.ProcessName))
                {
                    Console.WriteLine("+\tmoving: " + process.MainWindowTitle);
                    // Getting the Windows Handle
                    IntPtr id = process.MainWindowHandle;
                    // Getting dimensions of Window
                    RECT rct;
                    int width = 800; int height = 600;
                    if(GetWindowRect(id, out rct))
                    {
                        width = rct.Right - rct.Left;
                        height = rct.Bottom - rct.Top;
                    }
                    Program.MoveWindow(id, 100, 100, width, height, true);
                }
            }
        }
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }
    }
}