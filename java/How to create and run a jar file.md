A very simple approach to create .class, .jar file.

Executing the jar file. No need to worry too much about manifest file. Make it simple and elgant.

**Java sample Hello World Program**
    
    public class HelloWorld {
    	public static void main(String[] args) {
    		System.out.println("Hello World");
    	}
    }

**Compiling the class file**

    javac HelloWorld.java


**Creating the jar file**

The `jar's` option `e` lets you specify the `Main-Class` (entrypoint) and also creates a corresponding manifest file for you:

    jar cvfe HelloWorld.jar HelloWorld HelloWorld.class

 or

    jar cvfe HelloWorld.jar HelloWorld *.class

**Running the jar file**

     java -jar HelloWorld.jar
Or

    java -cp HelloWorld.jar HelloWorld

See the [official documentation for jar](https://docs.oracle.com/javase/8/docs/technotes/tools/windows/jar.html).
