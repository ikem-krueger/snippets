public class CallByRef {
	private static class WrappedInt {
		public int value;
		public WrappedInt(int value) {
			this.value = value;
		}
	}
	
	public static void process(WrappedInt i) {
		i.value++;
	}
	
	public static void main(String args[]){
		WrappedInt i = new WrappedInt(5);
		System.out.println("Value before: " + i.value);
		process(i);
		System.out.println("Value after: " + i.value);
	}
}
