@Test
public void addition() {
    assertEquals(12, simpleMath.add(7, 5));
}

@Test
public void subtraction() {
    assertEquals(9, simpleMath.substract(12, 3));
}

@Before
public void runBeforeEveryTest() {
    simpleMath= newSimpleMath();
}

@After
public void runAfterEveryTest() {
    simpleMath= null;
}

@BeforeClass
public static void runBeforeClass() {
    // run for one time before all test cases
}

@AfterClass
public static void runAfterClass() {
    // run for one time after all test cases
}

@Test(expected= ArithmeticException.class)
publicvoiddivisionWithException() {
    simpleMath.divide(1, 0);   // dividebyzero
}

@Test(timeout = 1000)
public void infinity() {
    while (true);
}

@Ignore("Not Ready to Run")
@Testpublic
void multiplication() {
    assertEquals(15, simpleMath.multiply(3, 5));
}
