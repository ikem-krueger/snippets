int dTime = 2000; //delay time 

void setup(){ 
  DDRD = 0xFF; 
} 

void loop() { 
  PORTD = 0b10101010; 
  delay(dTime); 
  PORTD |= (1<<PD6); 
  delay(dTime); 
  PORTD ^= (1<<PD6)|(1<<PD7);
  delay(dTime);
  PORTD |= (1<<PD0)|(1<<PD2)|(1<<PD4); 
  delay(dTime); 
  PORTD = (PORTD >> 3); 
  delay(dTime); 
  PORTD &= ~((1<<PD0)|(1<<PD2)); 
  delay(dTime); 
  PORTD = ~PORTD; 
  delay(dTime);
 }
