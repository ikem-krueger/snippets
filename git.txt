git -c "https.proxy=<host>:<port>" clone <repo_url>
git add -p # add changes by patch
git add .
git am --signoff < a_file.patch
git apply --check a_file.patch
git apply --stat a_file.patch
git archive --remote='git@bitbucket.org:sheldon/ask-sheldon.com.git' master path/to/file/or/folder/in/repo --format=tar.gz > archived_folder.tar.gz # export git subfolder to archive
git bisect run
git blame <file> # print out a short commit id, the author, timestamp for each changed line
git branch -d beta # delete local branch "beta"
git branch -d the_local_branch # delete a local branch
git branch -m <newname> # rename the current branch
git branch -m <oldname> <newname> # rename a branch
git branch --track <remote/origin> <remote>
git bundle create repo.bundle master
git checkout -- <file> # discard changes in working directory
git checkout -- <file> # reset a single file which has the same name as a branch
git checkout --orphan empty-branch
git checkout --ours -- file1.txt # in a merge conflict: use our version of file1
git checkout --theirs -- file1.txt # in a merge conflict: use their version of file1
git checkout -B main # move HEAD and reset branch to current commit
git checkout -b develop origin/develop
git checkout -b gh-pages # branch for a basic GitHub page site
git checkout <branch> # change to another branch
git checkout <branch> <file>
git checkout <file> # reset a single file
git checkout <file> # restore files in working tree from index
git checkout <shasum> <path/to/file>
git checkout feature_1 -- path/to/file/iwant # checkout a single file from another commit or branch
git checkout stash@{0} -- <file> # restore a single file from stash
git cherry-pick <commit>
git clean -f
git clean -n # show what files would be removed
git clone --mirror remote-repository-you-wish-to-backup
git clone -b master ./repo.bundle myproj
git clone <repo_url> --branch <tag_name>
git clone <repo_url> --branch <tag_name> --single-branch
git clone https://github.com/john-doe/my_repo_name.wiki.git
git commit --allow-empty -m "root commit"
git commit --amend # edit the last commit message
git commit --amend --author="Author Name <email@address.com>" # fix the last commit author
git commit --amend --no-edit
git commit -m "Initial commit"
git config --global --add safe.directory * # disable safe repository checks
git config --global --edit
git config --global --unset <variable>
git config --global --unset core.excludesfile
git config --global -e # open your global .gitconfig file in a text editor
git config --global alias.last 'log -1 HEAD'
git config --global alias.timeline "log --oneline --graph --decorate"
git config --global alias.unstage "reset HEAD" # setup an alias
git config --global color.ui auto # color highlighted terminal output
git config --global core.autocrlf input
git config --global core.longpaths true # fix "filename too long" issue
git config --global core.safecrlf false # disable "LF will be replaced by CLRF" warning in git on Windows
git config --global credential.helper cache # cache the password in the memory
git config --global credential.helper store
git config --global diff.ignoreSubmodules all # ignore submodules
git config --global diff.ignoreSubmodules dirty # ignore dirty submodules
git config --global diff.ignoreSubmodules none
git config --global help.autocorrect 30 # add autocorrect to git
git config --global http.proxy http://proxy.domain.com:3128
git config --global https.proxy https://proxy.domain.com:3128
git config --global push.autoSetupRemote true
git config --global push.default simple # push only to active upstream branch
git config --global user.email "john.doe@gmail.com" # set git account email
git config --global user.name "John Doe" # set git account username
git config --list
git config --unset <variable> # reset variable
git config -f .gitmodules submodule.$path.update merge
git config credential.helper 'cache --timeout=900' # remember the password for 15 minutes
git diff --cached <file>
git diff --ignore-space-at-eol # ignore whitespace at end of lines
git diff --name-only master # compare all changed files on current branch with master branch and show file names only
git diff --name-only <commit_from> <commit_to> # list only the names of files that changed
git diff ":!db/irrelevant.php" # exclude file from git diff (windows)
git diff ':!db/irrelevant.php' # exclude file from git diff (linux)
git diff --staged # show staged changes
git diff -b
git diff -w
git diff <commit> # see the changes in a commit
git diff <commit_from> <commit_to>
git diff <commit_from> <commit_to> <file>
git diff <tag>..<tag> # show difference from tag to tag
git diff branch1..branch2
git extras --help
git fetch origin master:master
git help --config # show all possible config keys
git help -g # show helpful guides
git help commit
git ignore # list ignore files
git info # retrieve context information
git init
git init --bare /path/to/repo.git
git log # show the detailed commit history
git log --after="2016-01-01" --before="2016-01-05"
git log --all
git log --all --graph
git log --author="author_a" --author="author_b" --pretty="format:\"%ad\";\"%s\"" --date=format:"%d.%m.%y %H:%M:%S" # search for commits from author_a and author_b, output as csv format
git log --author="enki\|Joe" # search for the commits made by "enki" or "Joe"
git log --name-only # full path names of changed files
git log --name-status # full path names and status of changed files
git log --oneline #  show commit comments
git log --oneline --graph --decorate # great visual graph
git log --oneline <file> # view the change history of a file
git log --since="last month" # limit log output by time
git log --stat # abbreviated pathnames and a diffstat of changed files
git log -1 # show the last 1 commit
git log -p <file> # generate the patches for each log entry
git ls-files
git ls-files --others --exclude-standard -z|xargs -0 tar rvf ~/backup-untracked.zip # backup of untracked files
git ls-tree -r <branch> --names-only
git merge --strategy-option ours
git merge --strategy-option theirs
git mr <number> # check out a merge request
git pr <number> # check out a pull request
git pull --all
git pull <git_url> <branch> --allow-unrelated-histories
git pull origin master # update local copy
git pull origin master --allow-unrelated-histories
git push --all origin # push all branches
git push --set-upstream origin master
git push --tags
git push --tags # push the tags to github
git push -f --mirror # rename the branch on the remote
git push -u origin master # replace master with the name of the branch you want to push
git push <remote_name> --delete <branch_name>
git push origin --delete test
git push origin -u newname
git push origin :beta # delete remote branch "beta"
git push origin :oldname newname
git push origin :refs/tags/1.28-5 # remove the tag from github
git push origin :the_remote_branch # remove a remote branch
git push origin empty-branch
git push origin tagname
git rebase --interactive # edit all commit messages altogether
git rebase --onto new-source old-source my-branch # avoid problems if the new source branch has had its history rewritten
git rebase -i --root
git release 0.1.0 -m <message>
git remote -v # list the existing remotes
git remote add local /var/blah # push to local repository (can be on a network share)
git remote add origin <url>
git remote add origin https://github.com/john-doe/my_repo_name.git
git remote add upstream https://location/of/generic.git
git remote remove origin
git remote rename origin enki
git remote set-url <name> <newurl> <oldurl>
git remote set-url origin https://hostname/USERNAME/REPOSITORY.git # change the remotes url
git remote show origin # getting remote code without overriding local files
git remote update --prune # branches that were deleted on the original will be deleted in the mirror
git reset # undo "git add"
git reset --hard HEAD
git reset --hard origin/master # force overwrite of untracked working tree files
git reset --merge ORIG_HEAD # undo merge that hasn't been pushed
git reset <file> # undo "git add" for a file
git reset HEAD~1 # undo last commit
git restore <file>
git restore --staged <file>
git restore --staged .
git rev-list --all | xargs git grep -F <term> # search
git revert <commit>
git revert HEAD
git rm --cached <file> # undo add of file
git show <shasum>
git show <shasum>:<path/to/file> > newpath/to/file # checkout a file from a certain commit
git show main:README.md # view a file of another branch
git show master:<path/to/file> > newpath/to/file #  checkout a file from a certain branch
git sparse-checkout disable
git sparse-checkout list
git sparse-checkout set <folder/file>
git stash # stash your changes
git stash -u # stash untracked changes
git stash <branch> <new_branch> # apply the stashed changes to a new branch before merging them into the old one
git stash apply # restore your changes
git stash apply stash@{1} # apply the second stash
git stash clear # delete all stashes
git stash drop <id> # delete a stash
git stash list # show all stashes
git stash pop # apply the top stash and delete it from the stack
git stash save <name> # name your stash
git stash show -p
git stash show -p stash@{1}
git stash show -p|git apply # restore untracked files from stash
git status
git status --short
git submodule add -b master git@github.com:Bla-bla/submodule.git sm_dir
git submodule foreach git status
git submodule update --init --recursive --remote
git tag --delete tagname
git tag -a v1.0.0 # create an "annotated" tag
git tag -a v1.0.0 -m "Releasing version v1.0.0"
git tag -a v1.0.0 -m "release version 1.0.0" # create github release
git tag -d 1.28-5 # delete a tag
git tag -l # list tags only
git tag -n # list tags including annotations
git tag v1.0.0 <commit> # create a "lightweight" tag
git switch --orphan <new branch> # create empty branch
git update
git update-index --assume-unchanged <filename> # ignore changes on <filename>
git update-index --no-assume-unchanged <filename> # track changes on <filename>
git worktree add <folder> <branch>
git worktree list
git worktree remove <folder>
octogit create <repo> 'description' # create the repository both locally and on github
octogit issues # see all the related issues in the current repository
octogit issues <number> # see a specific issue with summary
octogit issues <number> close # close an issue
octogit login # store your github authentication data
