#!/bin/sh

rm -r /Library/Extensions/CUDA.kext
rm -r /Library/Frameworks/CUDA.framework
rm -r /Library/LaunchAgents/com.nvidia.CUDASoftwareUpdate.plist
rm -r /Library/LaunchDaemons/com.nvidia.cuda.launcher.plist
rm -r /Library/LaunchDaemons/com.nvidia.cudad.plist
rm -r /Library/PreferencePanes/CUDA Preferences.prefPane
rm -r /usr/local/cuda
