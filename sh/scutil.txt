scutil --set HostName <new host name> # change the fully qualified hostname, for example "myMac.domain.com" 
scutil --set LocalHostName <new host name> # change the local network name, for example "myMac.local"
scutil --set ComputerName <new name> # change the computer name, for example "myMac"
dscacheutil -flushcache # flush the dns cache
