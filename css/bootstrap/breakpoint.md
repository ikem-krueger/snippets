**Update for Bootstrap 5 (2020)**

Bootstrap 5 (currently alpha) has a new **xxl breakpoint**. Therefore [display classes](https://v5.getbootstrap.com/docs/5.0/utilities/display/) have a new tier to support this:

Hidden only on xxl: `d-xxl-none`  
Visible only on xxl: `d-none d-xxl-block`

**Bootstrap 4 (2018)**

The `hidden-*` and `visible-*` classes _**no longer exist**_ in Bootstrap 4\. If you want to hide an element on specific tiers or breakpoints in Bootstrap 4, use the `d-*` [**display classes**](https://getbootstrap.com/docs/4.0/utilities/display/) accordingly.

Remember that extra-small/mobile (formerly `xs`) is the default (implied) breakpoint, unless overridden by a _larger_ breakpoint. Therefore, **the `-xs` infix no longer exists in Bootstrap 4**.

Show/hide for **breakpoint and down**:

*   `hidden-xs-down (hidden-xs)` = `d-none d-sm-block`
*   `hidden-sm-down (hidden-sm hidden-xs)` = `d-none d-md-block`
*   `hidden-md-down (hidden-md hidden-sm hidden-xs)` = `d-none d-lg-block`
*   `hidden-lg-down` = `d-none d-xl-block`
*   `hidden-xl-down` (n/a 3.x) = `d-none` (same as `hidden`)

Show/hide for **breakpoint and up**:

*   `hidden-xs-up` = `d-none` (same as `hidden`)
*   `hidden-sm-up` = `d-sm-none`
*   `hidden-md-up` = `d-md-none`
*   `hidden-lg-up` = `d-lg-none`
*   `hidden-xl-up` (n/a 3.x) = `d-xl-none`

Show/hide **only for a single breakpoint**:

*   `hidden-xs` (only) = `d-none d-sm-block` (same as `hidden-xs-down`)
*   `hidden-sm` (only) = `d-block d-sm-none d-md-block`
*   `hidden-md` (only) = `d-block d-md-none d-lg-block`
*   `hidden-lg` (only) = `d-block d-lg-none d-xl-block`
*   `hidden-xl` (n/a 3.x) = `d-block d-xl-none`
*   `visible-xs` (only) = `d-block d-sm-none`
*   `visible-sm` (only) = `d-none d-sm-block d-md-none`
*   `visible-md` (only) = `d-none d-md-block d-lg-none`
*   `visible-lg` (only) = `d-none d-lg-block d-xl-none`
*   `visible-xl` (n/a 3.x) = `d-none d-xl-block`

[Demo of the responsive display classes in Bootstrap 4](https://www.codeply.com/go/bRlHp8MxtJ)

**Also,** note that `d-*-block` can be replaced with **`d-*-inline`**, **`d-*-flex`**, **`d-*-table-cell`**, **`d-*-table`** etc.. depending on the display type of the element. Read more on the [display classes](https://getbootstrap.com/docs/4.0/utilities/display/)
